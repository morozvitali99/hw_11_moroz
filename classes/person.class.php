<?php
// наследуемый класс
    class PersonClass
    {
            public  $full_name = '';
            public  $email = '';
            public  $phone = '';
            public  $role_ = '';

    public function __construct(    $full_name,
                                    $email,
                                    $phone,
                                    $role_)
        {
            $this->full_name    =   $full_name;
            $this->email        =   $email;
            $this->phone        =   $phone;
            $this->role_        =   $role_;
        }

// тут напишем текст визитки одной строкой
    public function getVisitCard()
        {
        return  ' Email: ' . $this->email .
                ' Phone:' . $this->phone .
                ' Role: ' . $this->role_;
        }
    }
?>