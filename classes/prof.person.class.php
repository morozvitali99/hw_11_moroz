<?php
// наследующий класс преподаватели

class ProfPersonClass extends PersonClass{
    
    public $subject_ = '';

    public function __construct(    $full_name,
                                    $email,
                                    $phone,
                                    $role_,
                                    $subject_){
                                      
    parent::__construct(            $full_name,
                                    $email,
                                    $phone,
                                    $role_);

    $this->subject_ = $subject_;
// вывод визитки
    }
    public function getVisitCard(){
        echo    '<div class="card text-white bg-primary mb-3">';
        echo        '<div class="card-header">';
        echo            $this->full_name;
        echo                    '</div>';
        echo           '<div class="card-body">';
        echo            '<h5 class="card-title">';
        echo                'Professor Card';
        echo                '</h5>';
        echo                '<p class="card-text">'; 
        echo                parent::getVisitCard() . ' Subject: ' . $this->subject_;
        echo                ' </p>';
        echo             '</div>';
        echo             '</div>';
    }
}
?>